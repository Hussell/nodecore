msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-03-27 05:07+0200\n"
"PO-Revision-Date: 2024-08-24 17:23+0000\n"
"Last-Translator: minitob-qwq <minitob114514@hotmail.com>\n"
"Language-Team: Chinese (Simplified) <https://hosted.weblate.org/projects/"
"minetest/nodecore/zh_Hans/>\n"
"Language: zh_Hans\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 5.7.1-dev\n"

msgid "- @1"
msgstr "- @1"

msgid "- Aux+drop any item to drop everything."
msgstr "- 辅助键+丢弃来丢弃所有物品。"

msgid "- Be wary of dark caves/chasms; you are responsible for getting yourself out."
msgstr "- 注意洞穴和深坑；你自己想办法怎么出来。"

msgid "- Can't dig trees or grass? Search for sticks in the canopy."
msgstr "- 挖不了树和玻璃？在树冠上找找树枝。"

msgid "- Climbing spots may be climbed once black particles appear."
msgstr "- 出现黑色颗粒，就可以爬上攀登点。"

msgid "- Crafting is done by building recipes in-world."
msgstr "- 在世界中建造合成模式来合成物品。"

msgid "- DONE: @1"
msgstr "- 完成: @1"

msgid "- Displaced nodes can be climbed through like climbing spots."
msgstr "- 移位的节点可以像攀登点一样被爬过去。"

msgid "- Do not use F5 debug info; it will mislead you!"
msgstr "- 不要使用F5调试信息，会误导你的！"

msgid "- Drop and pick up items to rearrange your inventory."
msgstr "- 丢弃然后拿起物品来重排你的背包。"

msgid "- For larger recipes, the center item is usually placed last."
msgstr "- 在较大的合成模式中，中间的物品一般最后放置。"

msgid "- "Furnaces" are not a thing; discover smelting with open flames."
msgstr "- 没有“熔炉”这个东西；试着用明火熔炼。"

msgid "(C)2018-2021 by Aaron Suen <warr1024@@gmail.com>"
msgstr "(C)2018-2021 作者 Aaron Suen <warr1024@@gmail.com>"

msgid "- Climbing spots also produce very faint light; raise display gamma to see."
msgstr "- 攀登点也会产生非常微弱的光线；提高显示伽马查看。"

msgid "- Drop items onto ground to create stack nodes. They do not decay."
msgstr "- 将物品扔到地上，形成堆方块。堆方块不会腐烂。"

msgid "(C)2018-@1 by Aaron Suen <warr1024@@gmail.com>"
msgstr "(C)2018-@1 作者 Aaron Suen <warr1024@@gmail.com>"

msgid "- Hold/repeat right-click on walls/ceilings barehanded to create climbing spots."
msgstr "- 徒手按住/重复右键点击墙壁/天花板，创建攀爬点。"

msgid "- FUTURE: @1"
msgstr "- 未来: @1"

msgid "- Hopelessly stuck? Try asking the community chatrooms (About tab)."
msgstr "- 卡住了？试着问一下我们社区的聊天室吧（关于选项卡）."

msgid "- If a recipe exists, you will see a special particle effect."
msgstr "- 如果存在配方，您将看到特殊的粒子效果。"

msgid "- If it takes more than 5 seconds to dig, you don't have the right tool."
msgstr "- 如果都用了5秒多还没挖完，那你就得试试别的工具了."

msgid "- Items picked up try to fit into the current selected slot first."
msgstr "- 拾取的物品会尽量先放入当前选定的物品栏中。"

msgid "- Larger recipes are usually more symmetrical."
msgstr "- 更好的方法通常更适合."

msgid "About"
msgstr "关于"

msgid "- Nodes dug without the right tool cannot be picked up, only displaced."
msgstr "-挖掘方块没有正确的工具不能获得物品，只能破坏并且不会掉落。"

msgid "- Ores may be hidden, but revealed by subtle clues in terrain."
msgstr "- 矿石可能不会是裸露的，但位置会被地形的细微线索所揭示。"

msgid "- Recipes are time-based, punching faster does not speed up."
msgstr "配方取决于时间，更快的节奏并不会加速制作。"

msgid "- Sneak+aux+drop an item to drop all matching items."
msgstr "- 同时按下潜行键，辅助键丢弃物品会丢出所有与之相同的物品。"

msgid "- Sneak+drop to count out single items from stack."
msgstr "- 按下潜行键丢弃物品每次只会丢出一个物品。"

msgid "- Some recipes require "pummeling" a node."
msgstr "- 一些制作配方需要 “击打” 一个方块。"

msgid "- Something seems tedious? Find better tech, subtle factors, or a better way."
msgstr "- 有些事情似乎很乏味？使用更好的技术，掌控微妙的因素，或找到更好的办法。"

msgid "- Stacks may be pummeled, exact item count may matter."
msgstr "- 堆栈可能会受到猛击，精确的数量可能会有所影响。"

msgid "- The game is challenging by design, sometimes frustrating. DON'T GIVE UP!"
msgstr "- 这个子游戏被设计得非常具有挑战性，有时会令人沮丧。千万不要放弃！！！"

msgid "- There is NO inventory screen."
msgstr "- 这里不显示物品栏界面。"

msgid "- To pummel, punch a node repeatedly, WITHOUT digging."
msgstr "- 重复击打一个方块，不使用挖掘的方法。"

msgid "- To run faster, walk/swim forward or climb/swim upward continuously."
msgstr "- 持续向前或向上移动会越来越快。"

msgid "- Tools used as ingredients must be in very good condition."
msgstr "- 用作原料的工具必须保持完好。"

msgid "- Trouble lighting a fire? Try using longer sticks, more tinder."
msgstr "- 生火时遇到了麻烦？尝试使用更长的木棍和更多的引火物。"

msgid "- Wielded item, target face, and surrounding nodes may matter."
msgstr "-手中的物品，目标的脸和周围的方块对交互来说可能很重要。"

msgid "@1 (@2)"
msgstr "@1（@2）"

msgid "@1 ....."
msgstr "@1....."

msgid "- Order and specific face of placement may matter for crafting."
msgstr "- 特定的顺序和放置方向对合成来说很重要。"

msgid "- Learn to use the stars for long distance navigation."
msgstr "- 学会使用星星进行远距离导航。"

msgid "- You do not have to punch very fast (about 1 per second)."
msgstr "- 你不需要击打得很快（每秒一次即可）。"

msgid "@1 discovered, @2 available, @3 future"
msgstr "@1 被发现的，@2可用的，@3将来的"

msgid "@1 rejected. (protocol version @2)"
msgstr "@1 拒绝。（协议版本 @2）"

msgid "@1 |...."
msgstr "@1 |...."

msgid "@1 ||..."
msgstr "@1 ||..."

msgid "@1 |||.."
msgstr "@1 |||.."

#, fuzzy
msgid "Active Lens"
msgstr "活跃的镜头"

#, fuzzy
msgid "Active Prism"
msgstr "活跃的棱镜"

msgid "Additional Mods Loaded: @1"
msgstr "附加 Mod 已载入：@1"

msgid "Adobe"
msgstr "黏土坯"

msgid "Adobe Mix"
msgstr "混合黏土"

msgid "Aggregate"
msgstr "混凝料"

msgid "Amalgamation"
msgstr "混合"

msgid "Admin Tool"
msgstr "管理员工具"

msgid "@1 ||||."
msgstr "@1 ||||."

msgid "@1 |||||"
msgstr "@1 |||||"

msgid "Adobe Bricks"
msgstr "风干砖坯"

msgid "~ @1% translated to your language (@2)"
msgstr "～@1%翻译成你的语言（@2）"

msgid "write on a surface with a charcoal lump"
msgstr "用木炭在地上写字"

msgid "wet a concrete mix"
msgstr "打湿混凝土混合料"

msgid "wilt a flower"
msgstr "让一朵花凋零"

msgid "weaken stone by soaking"
msgstr "泡水软化石头"

msgid "throw an item really fast"
msgstr "迅速投掷物品"

msgid "squeeze out a sponge"
msgstr "拧干海绵"
