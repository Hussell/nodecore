msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-01-01 12:10+0200\n"
"PO-Revision-Date: 2024-01-02 12:06+0000\n"
"Last-Translator: Unacceptium <unacceptium@proton.me>\n"
"Language-Team: Hungarian <https://hosted.weblate.org/projects/minetest/"
"nodecore/hu/>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.4-dev\n"

msgid "- Climbing spots also produce very faint light; raise display gamma to see."
msgstr ""
"- a mászópontok egy nagyon gyenge fényt adnak ki; emeld feljebb a Gammát, "
"hogy lásd."

msgid "- @1"
msgstr "- @1"

msgid "- DONE: @1"
msgstr "- KÉSZ: @1"

msgid "- Can't dig trees or grass? Search for sticks in the canopy."
msgstr "- nem tudsz fákat, bokrokat kiütni? keress botokat a lombkoronában."

msgid "- Be wary of dark caves/chasms; you are responsible for getting yourself out."
msgstr ""
"- vigyázz a sötét bányákkal/szakadékokkal; te nekd kell magadat kijuttatnod "
"onnan."

msgid "- FUTURE: @1"
msgstr "- JÖVŐ: @1"

msgid "(C)2018-@1 by Aaron Suen <warr1024@@gmail.com>"
msgstr "(C)2018-@1 Aaron Suen-től <warr1024@@gmail.com>"

msgid "- Drop items onto ground to create stack nodes. They do not decay."
msgstr "- Dobj el több tárgyat a csoportosításukhoz. nem tünnek el."

msgid "- Climbing spots may be climbed once black particles appear."
msgstr ""
"- a mászópontok akkor mászhatók, amint fekete részecskék lebegnek körülöttük."

msgid "- Hopelessly stuck? Try asking the community chatrooms (About tab)."
msgstr "- Reménytelenül elakadtál? kérdezz a közösségi szobákban (Névjegy fül)."

msgid "- If it takes more than 5 seconds to dig, you don't have the right tool."
msgstr "- ha több, mint 5 másodperc kiütni, lehet nemj ó tárgyal próbálod."

msgid "- Aux+drop any item to drop everything."
msgstr "- Aux+eldobással mindent eldobsz."

msgid "- If a recipe exists, you will see a special particle effect."
msgstr "- ha létezik a recept, speciális részecskéket fogsz látni."

msgid "- Hold/repeat right-click on walls/ceilings barehanded to create climbing spots."
msgstr ""
"- tartsd nyomva/nyomogasd a jobb egérgombot puszta kézzel falon vagy "
"plafonon mászópontok készítéséhez."

msgid "- Displaced nodes can be climbed through like climbing spots."
msgstr "- félrerakott blokkok megmászhatók, úgy mint a mászópontok."

msgid "- "Furnaces" are not a thing; discover smelting with open flames."
msgstr "- \"Kemencék\" itt nincenek; fedezd fel a nyilt lángal való olvasztást."

msgid "- Crafting is done by building recipes in-world."
msgstr "- a kraftoláshoz meg kell építened a recepted a valóságban."

msgid "- For larger recipes, the center item is usually placed last."
msgstr "- Nagyobb recepteknél a középső tárgyat sokás utoljára rakni."

msgid "- Drop and pick up items to rearrange your inventory."
msgstr "- dobd el, majd vedd fel a tárgyakat az átrendezésükhöz."
